﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Text.RegularExpressions

Module basMain
    Sub Main()
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Try
            Dim HttpLis As New HttpListener()
            Dim strPrefix As String = My.Settings.httpPrefix
            Dim strStatusCode As String = My.Settings.httpStatusCode
            Dim strContentType As String = My.Settings.httpContentType

            HttpLis.Prefixes.Add(strPrefix)
            HttpLis.Start()

            Call subOutMessage(RET_NORMAL, "！！！食券購入情報登録システム START！！！", strMethod)

            While (True)
                Dim strContext As HttpListenerContext = HttpLis.GetContext
                Dim strReq As HttpListenerRequest = strContext.Request
                Dim strRes As HttpListenerResponse = strContext.Response
                Dim bytRes As Byte()
                Dim strWork As String = vbNullString
                Dim objOutput As System.IO.Stream = strRes.OutputStream
                Dim objInput As System.IO.StreamReader
                objInput = New StreamReader(strReq.InputStream)
                Dim strData As String = vbNullString
                Dim intPos As Integer = 0
                Dim intRet As Integer = 0
                Dim strEdit As String = objInput.ReadToEnd
                Dim html As String = ""
                Call subOutMessage(RET_NORMAL, "受信情報：" & strEdit, strMethod)

                '応答データを受信するためのStreamを取得
                With typMiFare
                    .CID = strEdit.Substring(InStr(strEdit, "val") + 8, 8)
                    strWork = strEdit.Substring(InStr(strEdit, "time") + 6, 14)
                    .READ_DATE = strWork.Substring(0, 8)
                    .READ_TIME = strWork.Substring(8, 6)
                    .TID = strEdit.Substring(InStr(strEdit, "tid") + 6, 12)
                    Call subOutMessage(RET_NORMAL, "データ展開　：チップ番号=" & .CID & ",カード読込日=" & .READ_DATE & ",カード読込時刻=" & .READ_TIME & ",TID=" & .TID, strMethod)

                    intRet = ChkCID(typMiFare)
                    Select Case intRet
                        Case RET_NOTFOUND
                            Call subOutMessage(RET_NORMAL, "カード情報無　：チップ番号=" & .CID & ",カード読込日=" & .READ_DATE & ",カード読込時刻=" & .READ_TIME & ",TID=" & .TID, strMethod)
                        Case RET_WARNING
                        Case RET_ERROR
                    End Select
                End With

                strRes.StatusCode = strStatusCode
                strRes.ContentType = strContentType
                strWork = My.Settings.httpRes.Replace(",", vbCrLf)
                bytRes = System.Text.Encoding.UTF8.GetBytes(strWork)
                'objOutput.Write(bytRes, 0, bytRes.Length)
                'objOutput.Close()
                strRes.OutputStream.Write(bytRes, 0, bytRes.Length)
                strRes.OutputStream.Close()
                Call subOutMessage(RET_NORMAL, "レスポンス応答", strMethod)

            End While
        Catch ex As Exception
            Call subOutMessage(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
        End Try
    End Sub

    Public Function ChkCID(ByVal objType As TYPE_MIFARE) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim objManurty As New d_prim_manurtyDataSet1TableAdapters.GetUsersTableAdapter
        Dim dtManurty As New d_prim_manurtyDataSet1.GetUsersDataTable
        Dim strTerminal As String = vbNullString
        Dim strFoodName As String = vbNullString
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim dsPurchase As New DataSet

        '---Ver.1.2.0 UpDated By Watanabe 2020.12.04
        '---旧社員証使用考慮のため、d_prim_cocowa_icから旧社員証情報を取得。このDBになければ、社員証じゃないと判断する。
        Dim objCocowaIC As New d_prim_cocowa_icDataSetTableAdapters.t_try_logTableAdapter
        Dim dtIC As New d_prim_cocowa_icDataSet.t_try_logDataTable
        Dim intFlg As Integer = 0


        Try
            'objManurty.FillByIC(dtManurty, objType.CID.ToUpper)

            'If dtManurty.Rows.Count <= 0 Then

            '    Call subOutMessage(RET_NORMAL, "読込カード情報未存在：CID = " & objType.TID & Space(1) & "タイムレコーダ用社員証情報検索", strMethod)


            '---START Ver.1.2.0 UpDated By Watanabe 2020.12.04
            '---旧社員証情報保持の可能性もあるのでタイムレコーダ用DBを検索
            objCocowaIC.FillByIC(dtIC, objType.CID.ToUpper)

                If dtIC.Rows.Count <= 0 Then
                    '---未存在の場合は社員証じゃないと判断
                    Return RET_NOTFOUND
                Else
                    Call subOutMessage(RET_NORMAL, "カード情報存在：CID = " & objType.CID & "、Users_id = " & dtIC.Rows(0).Item("users_id"), strMethod)

                    '---社員証情報のユーザIDでd_prim_ManurtyのUsersを検索
                    objManurty.FillByUserId(dtManurty, dtIC.Rows(0).Item("users_id"))

                    If dtManurty.Rows.Count <= 0 Then
                        Call subOutMessage(RET_NORMAL, "カード情報Manurty未存在：CID = " & objType.CID & "、Users_id = " & dtIC.Rows(0).Item("users_id"), strMethod)
                        '---未存在ならば、無効社員証として判断
                        Return RET_NOTFOUND
                    End If
                End If
            '---END Ver.1.2.0 UpDated By Watanabe 2020.12.04
            'End If

            With objType
                Call subOutMessage(RET_NORMAL, "START 購入履歴情報登録", strMethod)
                Select Case .TID
                    Case My.Settings.A_TERMINAL
                        strFoodName = My.Settings.A_TERMINAL_NO
                    Case My.Settings.B_TERMINAL
                        strFoodName = My.Settings.B_TERMINAL_NO
                    Case My.Settings.C_TERMINAL
                        strFoodName = My.Settings.C_TERMINAL_NO

                End Select

                Call fncDBConnect()

                '---START 1日での購入上限を超えた場合、登録せず。※購入上限が0の場合は無視。 2020.11.24 ADD By Watanabe
                If My.Settings.MAX_PURCHASE > 0 Then
                    strSQL.Clear()
                    strSQL.AppendLine("SELECT COUNT(HistoryAno) AS CountPurchase")
                    strSQL.AppendLine("FROM trnBuyHistory")
                    strSQL.AppendLine("WHERE buyCID = '" & .TID & "'")
                    strSQL.AppendLine("AND buyDate = '" & .READ_DATE & "'")
                    dsPurchase = fncAdptSQL(objConnect_LocalDB, strSQL.ToString)
                    If dsPurchase.Tables(0).Rows.Count > My.Settings.MAX_PURCHASE Then
                        Call subOutMessage(RET_NORMAL, "購入済み：CID = " & .CID & Space(1) & ",職員番号 = " & dtManurty.Rows(0).Item("userid") & ",氏名 = " & dtManurty.Rows(0).Item("name"), strMethod)
                        Return RET_NORMAL
                    End If
                End If
                '---END 2020.11.24 ADD By Watanabe

                strSQL.Clear()
                strSQL.AppendLine("SELECT CAST(MenuAno AS VARCHAR) AS MenuAno,MenuName,Price")
                strSQL.AppendLine("FROM")
                strSQL.AppendLine("MstMenu")
                strSQL.AppendLine("WHERE TerminalID = '" & .TID & "'")
                strSQL.AppendLine("AND DelFlg = 'False'")
                dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString)
                .MENU_ANO = dsData.Tables(0).Rows(0).Item("MenuAno")

                strSQL.Clear()
                strSQL.AppendLine("INSERT INTO trnBuyHistory")
                strSQL.AppendLine("(")
                strSQL.AppendLine("buyCID,")
                strSQL.AppendLine("buyUserEmpCode,")
                strSQL.AppendLine("buyGroupCode,")
                strSQL.AppendLine("buySectionCode,")
                strSQL.AppendLine("buyUserID,")
                strSQL.AppendLine("buyUserName,")
                strSQL.AppendLine("buyUserKana,")
                strSQL.AppendLine("buyMenuName,")
                strSQL.AppendLine("buyPrice,")
                strSQL.AppendLine("buyDate,")
                strSQL.AppendLine("buyTime,")
                strSQL.AppendLine("buyType,")
                strSQL.AppendLine("MenuAno")
                strSQL.AppendLine(")")
                strSQL.AppendLine("VALUES")
                strSQL.AppendLine("(")
                strSQL.AppendLine("'" & .CID.ToUpper & "',")
                strSQL.AppendLine("'" & dtManurty.Rows(0).Item("empcode") & "',")
                strSQL.AppendLine("'" & dtManurty.Rows(0).Item("groupcode") & "',")
                strSQL.AppendLine("'" & dtManurty.Rows(0).Item("sectioncode") & "',")
                strSQL.AppendLine("'" & dtManurty.Rows(0).Item("userid") & "',")
                strSQL.AppendLine("'" & dtManurty.Rows(0).Item("name") & "',")
                strSQL.AppendLine("'" & dtManurty.Rows(0).Item("nameKana") & "',")
                strSQL.AppendLine("'" & dsData.Tables(0).Rows(0).Item("MenuName") & "',")
                strSQL.AppendLine("'" & dsData.Tables(0).Rows(0).Item("Price") & "',")
                strSQL.AppendLine("'" & .READ_DATE & "',")
                strSQL.AppendLine("'" & .READ_TIME & "',")
                strSQL.AppendLine("'" & strFoodName & "',")
                strSQL.AppendLine("" & .MENU_ANO & "")
                strSQL.AppendLine(")")

                Call fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString)

                Call subOutMessage(RET_NORMAL, "購入履歴情報登録：" & "CID = " & .CID & Space(1) & "、職員番号 = " & dtManurty.Rows(0).Item("userid") & "、職員氏名 = " & dtManurty.Rows(0).Item("name") & "(" & dtManurty.Rows(0).Item("nameKana") & ")", strMethod)
                Call subOutMessage(RET_NORMAL, "END 購入履歴情報登録", strMethod)

            End With

            Return RET_NORMAL
        Catch ex As Exception
            Call subOutMessage(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            Call subOutMessage(RET_ERROR, "SQL：" & strSQL.ToString, strMethod)

            Return RET_ERROR
        Finally
            Call fncDBDisConnect()
        End Try
    End Function
End Module
